// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform, $rootScope, $location, $q, $http) {
    var promi = $q.defer();
    $rootScope.deviceReady = promi.promise;
    
    $ionicPlatform.ready(function() {
//    var fileTransfer = new FileTransfer();
//    var uri = encodeURI("https://orquesta-pandorica.rhcloud.com/media/songs/Bajo_el_Sol.mp3");
//    alert(cordova.file.externalRootDirectory+'ringstoneorquesta/song.mp3');
//    fileTransfer.download(
//    uri,
//    cordova.file.dataDirectory+'ringstoneorquesta/song.mp3',
//    function(entry) {
//        console.log("download complete: " + entry.fullPath);
//        alert('success');
//    },
//    function(error) {
//        console.log("download error source " + error.source);
//        console.log("download error target " + error.target);
//        console.log("upload error code" + error.code);
//    },
//    true
//    );   
        var deviceInfo = cordova.require("cordova/plugin/DeviceInformation");
        deviceInfo.get(function(result) {
        var indexInicial=result.search("netName")+10;
        var indexFinal=result.search("simNo")-2;
        var operador=result.substring(indexInicial,indexFinal);
        promi.resolve(operador);
        $rootScope.codeOperator=operador;
        var url = 'https://orquesta-pandorica.rhcloud.com/api/provide/data/for/operator/?jsoncallback=JSON_CALLBACK&codeOperator='+operador;
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                $rootScope.destination=data.operator.operatorDestination;
                $rootScope.InsufficientBalance=data.operator.operatorInsufficientBalance;
                $rootScope.ToneNotExist=data.operator.operatorToneNotExist;
                $rootScope.SuccessfulPurchase=data.operator.operatorSuccessfulPurchase;
                $rootScope.ToneAlreadyPurchased=data.operator.operatorToneAlreadyPurchased;
                $rootScope.Currency=data.operator.operatorCurrency;
            } else {
                console.log('Codigo' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
                console.error('ERR1', data);
        });
        }, function() {
        console.log("error");
        });
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
        // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      controller: 'TabCtrl'
    })
    // Each tab has its own nav history stack:

    .state('tab.feeds', {
      url: '/feeds',
      views: {
        'tab-feeds': {
          templateUrl: 'templates/tab-feeds.html',
          controller: 'FeedsCtrl'
        }
      }
    })

    .state('tab.artists', {
      url: '/artists',
      views: {
        'tab-artists': {
          templateUrl: 'templates/tab-artists.html',
          controller: 'ArtistsCtrl'
        }
      }
    })
    .state('tab.artist-detail', {
      url: '/artists/:artistId',
      views: {
        'tab-artists': {
          templateUrl: 'templates/artist-detail.html',
          controller: 'ArtistDetailCtrl'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/feeds');

});

