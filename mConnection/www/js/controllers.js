angular.module('starter.controllers', ['ngCordova.plugins.media'])
.controller('TabCtrl', function($scope, $ionicModal, $timeout, $ionicPlatform, $cordovaMedia, $http, $ionicPopup, $rootScope, $timeout, $ionicLoading, $state) {
    $scope.shuffleActive=true;
    $scope.sondId=-1;
    $scope.nowplaying = false;
    $scope.media = null;
    $scope.isPlaying = false;
    $scope.songPlaying = '';
    $scope.artist='';
    $scope.mediaSource=null;
    $scope.onTabSelected = function() {
        $state.go('tab.artists');
    }
    $ionicPlatform.onHardwareBackButton(function() {
        //$scope.showToast($state.current.views);
        if($state.current.views=='FeedsCtrl')
        {
            $scope.shuffleActive=false;
        };
    });
    $scope.savePurchaseLog=function(url)
    {
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                $scope.showToast('Guardado registro.');
            } else {
                console.log('Codigo es ' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
            console.error('ERR1', data);
        });
    }
    $scope.showToast= function(message)
    {
        window.plugins.toast.showShortBottom(message, null, null);
    }
    $scope.share=function(text)
    {
        text2='Personalizate con Backtones Claro. Escucha '+text+' en Music Connection. Descargada desde https://play.google.com';
        window.plugins.socialsharing.share(text2);
    }
	$scope.Enviando = function(senderNumber,songCode) {
		$ionicLoading.show({
			template: 'Enviando SMS... <i class="icon ion-loading-c"> ' 
		});
        var smsplugin = cordova.require("info.asankan.phonegap.smsplugin.smsplugin");
        function smsGestion(result)
        {
            if(result=='OK'){
                $ionicLoading.hide(); 
                $scope.Recibiendo(smsplugin);
            }else
            {
                alert('Error');
            }
        }
        smsplugin.send(senderNumber,songCode,smsGestion,smsGestion);
	};
	$scope.Recibiendo = function(smsplugin) {
        var flag=false;
        function reception(result)
        {
            if(result.indexOf('>')>-1 && !flag){        
                var arrayData=result.split('>');
                var origin = arrayData[0];
                var content = arrayData[1];
                var ToneNotExist=eval('(' + $rootScope.ToneNotExist + ')') ;
                var InsufficientBalance=eval('(' + $rootScope.InsufficientBalance + ')');
                var SuccessfulPurchase=eval('(' + $rootScope.SuccessfulPurchase + ')');
                var ToneAlreadyPurchased=eval('(' + $rootScope.ToneAlreadyPurchased + ')');
                if(origin == $rootScope.destination)
                {
                    if(ToneNotExist.test(content))
                    {
                        $scope.showToast('Tono no existe.');
                        smsplugin.stopReception(null,null);
                        flag=true;
                        $ionicLoading.hide();
                        $scope.savePurchaseLog('https://orquesta-pandorica.rhcloud.com/api/log/purchase/?jsoncallback=111&usuario=Mario&result='+'Tono no existe.');
                    }
                    if(InsufficientBalance.test(content))
                    {
                        $scope.showToast('Saldo insuficiente.');
                        $scope.savePurchaseLog();
                        smsplugin.stopReception(null,null);
                        flag=true;
                        $ionicLoading.hide();
                        $scope.savePurchaseLog('https://orquesta-pandorica.rhcloud.com/api/log/purchase/?jsoncallback=111&usuario=Mario&result='+'Saldo insuficiente.');
                    }
                    if(SuccessfulPurchase.test(content))
                    {
                        $scope.showToast('Tono comprado exitosamente.');
                        smsplugin.stopReception(null,null);
                        flag=true;
                        $ionicLoading.hide();
                        $scope.savePurchaseLog('https://orquesta-pandorica.rhcloud.com/api/log/purchase/?jsoncallback=111&usuario=Mario&result='+'Tono comprado exitosamente.');
                    }
                    if(ToneAlreadyPurchased.test(content))
                    {
                        $scope.showToast('Tono comprado anteriormente.');
                        smsplugin.stopReception(null,null);
                        flag=true;
                        $ionicLoading.hide();
                        $scope.savePurchaseLog('https://orquesta-pandorica.rhcloud.com/api/log/purchase/?jsoncallback=111&usuario=Mario&result='+'Tono comprado anteriormente.');
                    }
                }
                
            }
        }
		$ionicLoading.show({
			template: 'Esperando confirmación por SMS... <i class="icon ion-loading-c"> ',
            duration: 30000,
		});
		$timeout(function() {
            if(flag==false)
            {
                $ionicLoading.hide();
                window.plugins.toast.showShortBottom('no se recibe mensaje de confirmacion', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)});
                $scope.savePurchaseLog('https://orquesta-pandorica.rhcloud.com/api/log/purchase/?jsoncallback=111&usuario=Mario&result='+'no se recibe mensaje de confirmacion.');
                
            }
		}, 30000);
        smsplugin.startReception(reception,reception);   
		
	 };
	 $scope.hideLoading = function(){
		$ionicLoading.hide();
	 };
    // A confirm dialog
    $scope.showConfirmMain = function()
    {
        if($scope.songPlaying != '')
        {
            $scope.showConfirm($scope.songPlaying,$scope.songCode,$scope.songPrice);
        }
        else
        {
            $scope.showToast('seleccione una cancion');
        }
    }
	
    $scope.showConfirm = function(backtoneName, code, price) {
        var id = localStorage.getItem('id')||'None';
        if(id=='None')
        {
            $scope.showToast('Por favor registrarse.');
            $state.go('tab.account');
            return 0;
        }
        var confirmaCompra = $ionicPopup.confirm({
            title: 'Confirmar compra',
            subTitle: 'Precio: '+$rootScope.Currency+''+price,
            template: 'Está seguro que desea comprar el backtone <b>'+backtoneName+'</b>? \
					Se enviará un SMS para realizar la compra\
					<div class="list list-inset">\
						<label class="item item-input"> <span class="input-label">Destino</span><input type="tel" value="'+$rootScope.destination+'" disabled></label>\
						<label class="item item-input"> <span class="input-label">Texto</span><input type="text" value="'+code+'" disabled></label>\</div>',
              buttons: [{ 
                    text: 'Cancel',
                    type: 'button-mandarina button-outline',
                    onTap: function(e) {
                      // e.preventDefault() will stop the popup from closing when tapped.
                      //e.preventDefault();
                    }
                  }, {
                    text: 'Comprar',
                    type: 'button-mandarina',
                    onTap: function(e) {
				    $scope.Enviando($rootScope.destination,code);
                    }
                  }]
        });
    };
    $scope.changeShuffle= function()
    {
        $scope.shuffleActive= !$scope.shuffleActive;
    }
    
    //manejo de operador de telefonia
    $scope.playSong = function(songName, src, songId, songPrice, songCode, artist){
        $scope.artist=artist;
        $scope.sondId= songId;
        $scope.songPlaying = songName;
        $scope.songPrice=songPrice;
        $scope.songCode=songCode;
        $ionicPlatform.ready(function() {
            function mediaStatus(status)
            {
                // the end of the song
                if(status==4 && $scope.shuffleActive && $cordovaMedia.getDuration($scope.media)!=-1)
                {
                    var url = 'https://orquesta-pandorica.rhcloud.com/api/provide/random/song/?jsoncallback=JSON_CALLBACK&idSong='+$scope.sondId+'&codeOperator='+$rootScope.codeOperator;
                    $http.jsonp(url).success(function(data, status, headers, config) {
                        if(data.codigo == 1) {
                            $scope.playSong(data.song.songName,'https://orquesta-pandorica.rhcloud.com'+data.song.songUrl,data.song.songId,data.song.songPrice,data.song.songCode,data.artistName);
                        } else {
                            console.log('Codigo es ' + data.codigo);
                        }
                    }).error(function(data, status, headers, config) {
                        console.error('ERR1', data);
                    });
                }
            }
            $scope.mediaSource=$cordovaMedia.newMedia(src,null,null,mediaStatus);
            
            if($scope.nowplaying == true && $scope.media && $scope.media.src == src) {
                return 0;
            }
            if($scope.nowplaying == true && $scope.media && $scope.media.src != src) {
                $scope.media.release();
                $scope.media = $scope.mediaSource.media;
                $cordovaMedia.play($scope.media);
                $scope.isPlaying=true;
            }
            if($scope.nowplaying == false) {
                $scope.media = $scope.mediaSource.media;
                $cordovaMedia.play($scope.media);
                $scope.nowplaying = true;
                $scope.isPlaying=true;
            }
            
        });
    }
    $scope.pauseAudio = function() {
        $ionicPlatform.ready(function() {    
            if($scope.media) {
                if( $scope.nowplaying) {  
                    $cordovaMedia.pause($scope.media);
                    $scope.isPlaying = false;
                    $scope.nowplaying = false;
                }else{
                    $cordovaMedia.play($scope.media);
                    $scope.isPlaying = true;
                    $scope.nowplaying = true;
        
                }
               
            }
        });
    }
})
.controller('FeedsCtrl', function($scope, $http, $rootScope, $ionicPlatform) {
    $rootScope.deviceReady.then(function(operator){
        $scope.getFeedList = function() {
                var url = 'https://orquesta-pandorica.rhcloud.com/api/list/feeds/?jsoncallback=JSON_CALLBACK&codeOperator='+operator;
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                console.log('Codigo es 11');
                $scope.feeds = data.data;
                console.log(JSON.stringify($scope.feeds));
            } else {
                console.log('Codigo es ' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
            console.error('ERR1', data);
            $scope.feeds = [{
                artistId: 0,
                artistName: 'Error',
                artistImage: '/img/ionic.png'
            }];
        });
        };
        $scope.getFeedList();
        });    
})
.controller('ArtistsCtrl', function($scope, $http, $rootScope) {
    $scope.getArtistList = function() {
        var url = 'https://orquesta-pandorica.rhcloud.com/api/find/artists/?jsoncallback=JSON_CALLBACK&textToFind=&codeOperator='+$rootScope.codeOperator;
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                console.log('Codigo es 1');
                $scope.artistas = data.artists;
            } else {
                console.log('Codigo es ' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
            console.error('ERR1', data);
            $scope.artistas = [{
                artistId: 0,
                artistName: 'Error',
                artistImage: '/img/ionic.png'
            }];
        });
    };
    $scope.getArtistList();
}).controller('ArtistDetailCtrl', function($scope, $stateParams, $http, $rootScope) {
    artistId = $stateParams.artistId;
    console.log("PARAMETROS: " + artistId);
    $scope.getArtistSongs = function() {
        var url = 'https://orquesta-pandorica.rhcloud.com/api/find/songs/same/artist/?jsoncallback=JSON_CALLBACK&idArtist=' + artistId+'&codeOperator='+$rootScope.codeOperator;
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                console.log('Codigo es 1');
                $scope.detalleartista = data;
            } else {
                console.log('Codigo es ' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
            console.error('ERR1', data);
            $scope.artistas = [{
                artistId: 0,
                artistName: 'Error',
                artistImage: '/img/ionic.png'
            }];
        });
    };
    $scope.getArtistSongs();
}).controller('AccountCtrl', function($scope, $http, $ionicLoading, $timeout) {
    $scope.waitSMSForAutentication=function(smsPlugin,Token)
    {
        var flag=false;
        function reception(result)
        {
            alert(result);
            if(result.indexOf('>')>-1 && !flag){        
                var arrayData=result.split('>');
                var origin = arrayData[0];
                var content = arrayData[1];
                if(content==Token)
                {
                    $scope.showToast('Valido');
                }
                else
                {
                    $scope.showToast('No valido');
                }
                
            }
        }
		$ionicLoading.show({
			template: 'Esperando confirmación por SMS... <i class="icon ion-loading-c"> ',
            duration: 30000,
		});
		$timeout(function() {
            if(flag==false)
            {
                $ionicLoading.hide();
                $scope.showToast('No se recibe mensaje de confirmacion');
            }
		}, 30000);
        smsplugin.startReception(reception,reception);   
    }
    
    $scope.sendSMSForAutentication= function(Destination,Token)
    {
        $ionicLoading.show({
			template: 'Enviando SMS... <i class="icon ion-loading-c"> ' 
		});
        var smsplugin = cordova.require("info.asankan.phonegap.smsplugin.smsplugin");
        function smsGestion(result)
        {
            if(result=='OK'){
                $ionicLoading.hide();
                $scope.waitSMSForAutentication(smsplugin,Token);
            }else
            {
                alert('Error');
            }
        }
        smsplugin.send(Destination,Token,smsGestion,smsGestion);
    }
    $scope.showToast= function(message)
    {
        window.plugins.toast.showShortBottom(message, null, null);
    }
    $scope.data = [];
    $scope.data.user=localStorage.getItem('user') ||'';
    $scope.data.telephone=localStorage.getItem('telephone') ||'';
    $scope.data.email=localStorage.getItem('email') ||'';
    
    $scope.saveDataUser= function()
    {
        localStorage.setItem('user',$scope.data.user);
        localStorage.setItem('telephone',$scope.data.telephone);
        localStorage.setItem('email',$scope.data.email);
        if(true)
        {
               $scope.sendSMSForAutentication('0101','ATD');
        }
        var id = localStorage.getItem('id')||'None';    
        var url = 'https://orquesta-pandorica.rhcloud.com/api/save/user/data/?jsoncallback=JSON_CALLBACK&usuario='+$scope.data.user+'&email='+ $scope.data.email+'&telephone='+$scope.data.telephone+'&id='+id;
        $http.jsonp(url).success(function(data, status, headers, config) {
            if(data.codigo == 1) {
                localStorage.setItem('id',data.user);
                $scope.showToast('Ingresado correctamente.');
            } else {
                console.log('Codigo es ' + data.codigo);
            }
        }).error(function(data, status, headers, config) {
            console.error('ERR1', data);
        });
    }
});